﻿
namespace ORPG28_HOLA_MUNDO_TAREA_01
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tbprincipal = new System.Windows.Forms.TableLayoutPanel();
            this.btnhola = new System.Windows.Forms.Button();
            this.btnadiòs = new System.Windows.Forms.Button();
            this.lblmensaje = new System.Windows.Forms.Label();
            this.tbprincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbprincipal
            // 
            this.tbprincipal.ColumnCount = 2;
            this.tbprincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.45098F));
            this.tbprincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.54902F));
            this.tbprincipal.Controls.Add(this.btnhola, 0, 0);
            this.tbprincipal.Controls.Add(this.btnadiòs, 0, 1);
            this.tbprincipal.Controls.Add(this.lblmensaje, 1, 0);
            this.tbprincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbprincipal.Location = new System.Drawing.Point(0, 0);
            this.tbprincipal.Name = "tbprincipal";
            this.tbprincipal.RowCount = 2;
            this.tbprincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tbprincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tbprincipal.Size = new System.Drawing.Size(408, 204);
            this.tbprincipal.TabIndex = 0;
            // 
            // btnhola
            // 
            this.btnhola.BackColor = System.Drawing.Color.Indigo;
            this.btnhola.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnhola.FlatAppearance.BorderSize = 3;
            this.btnhola.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnhola.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnhola.ForeColor = System.Drawing.Color.White;
            this.btnhola.Location = new System.Drawing.Point(3, 3);
            this.btnhola.Name = "btnhola";
            this.btnhola.Size = new System.Drawing.Size(105, 96);
            this.btnhola.TabIndex = 0;
            this.btnhola.Text = "HOLA";
            this.btnhola.UseVisualStyleBackColor = false;
            this.btnhola.Click += new System.EventHandler(this.btnhola_Click);
            // 
            // btnadiòs
            // 
            this.btnadiòs.BackColor = System.Drawing.Color.Blue;
            this.btnadiòs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnadiòs.FlatAppearance.BorderSize = 3;
            this.btnadiòs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnadiòs.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnadiòs.ForeColor = System.Drawing.Color.White;
            this.btnadiòs.Location = new System.Drawing.Point(3, 105);
            this.btnadiòs.Name = "btnadiòs";
            this.btnadiòs.Size = new System.Drawing.Size(105, 96);
            this.btnadiòs.TabIndex = 1;
            this.btnadiòs.Text = "ADIÒS";
            this.btnadiòs.UseVisualStyleBackColor = false;
            this.btnadiòs.Click += new System.EventHandler(this.btnadiòs_Click);
            // 
            // lblmensaje
            // 
            this.lblmensaje.BackColor = System.Drawing.Color.Fuchsia;
            this.lblmensaje.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblmensaje.Font = new System.Drawing.Font("Arial Black", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmensaje.Location = new System.Drawing.Point(114, 0);
            this.lblmensaje.Name = "lblmensaje";
            this.tbprincipal.SetRowSpan(this.lblmensaje, 2);
            this.lblmensaje.Size = new System.Drawing.Size(291, 204);
            this.lblmensaje.TabIndex = 2;
            this.lblmensaje.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Pink;
            this.ClientSize = new System.Drawing.Size(408, 204);
            this.Controls.Add(this.tbprincipal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "hola mundo pcp 1-1";
            this.tbprincipal.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tbprincipal;
        private System.Windows.Forms.Button btnhola;
        private System.Windows.Forms.Button btnadiòs;
        private System.Windows.Forms.Label lblmensaje;
    }
}

